#
# Copyright (C) 2024 G. Allen Morris III
#
# This is free software, licensed under the Apache License
# See /LICENSE for more information.
#

include $(TOPDIR)/rules.mk

PKG_NAME:=netwatch
PKG_RELEASE:=1

PKG_SOURCE_PROTO:=git
#PKG_SOURCE_URL=$(PROJECT_GIT)/project/firewall3.git
PKG_SOURCE_DATE:=2024-01-24
PKG_SOURCE_VERSION:=4cd7d4f36bea731bf901cb067456f1d460294926
PKG_MIRROR_HASH:=ce9e8ac1bcf22afbb0a80c3da1a8e8e887851299681097e3dfbfc347f2c4c80f
PKG_MAINTAINER:=G. Allen Morris III <gam3@gam3.net>
PKG_LICENSE:=Apache-2.0

PKG_CONFIG_DEPENDS := fping, busybox

include $(INCLUDE_DIR)/package.mk

define Package/firewall
  SECTION:=net
  CATEGORY:=Network
  TITLE:=NetWatch
  DEPENDS:=+fping +busybox
  PROVIDES:=netwatch
  CONFLICTS:=
endef

define Package/netwatch/description
 This package provides a network watcher with led feedback
endef

define Package/netwatch/conffiles
/etc/config/netwatch
endef

CMAKE_OPTIONS += $(if $(CONFIG_IPV6),,-DDISABLE_IPV6=1)

define Package/netwatch/install
	$(INSTALL_DIR) $(1)/sbin
	$(INSTALL_BIN) ./sbin/netwatch $(1)/sbin/netwatch
	$(INSTALL_DIR) $(1)/etc/init.d
	$(INSTALL_BIN) ./files/netwatch.init $(1)/etc/init.d/netwatch
	$(INSTALL_DIR) $(1)/etc/config/
	$(INSTALL_CONF) ./files/netwatch.config $(1)/etc/config/netwatch
endef

$(eval $(call BuildPackage,firewall))
