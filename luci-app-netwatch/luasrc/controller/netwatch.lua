-- Copyright 2018 G. Allen Morris III
-- Licensed to the public under the Apache License 2.0.

module("luci.controller.netwatch", package.seeall)

function index()
	if not nixio.fs.access("/etc/config/netwatch") then
		return
	end

	local page

	page = entry({"admin", "services", "netwatch"}, cbi("netwatch"), _("Network Watch"))
	page.dependent = true
end
