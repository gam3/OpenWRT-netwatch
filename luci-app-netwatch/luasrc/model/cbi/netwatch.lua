-- Copyright 2018 G. Allen Morris III <gam3@gam3.net>
-- Licensed to the public under the Apache License 2.0.

m = Map("netwatch", translate("Network Shares"))

s = m:section(TypedSection, "netwatch", "NetWatch")
s.anonymous = true

s:tab("general",  translate("General Settings"))
s:tab("template", translate("Edit Template"))

s:taboption("general", Value, "router", translate("Router"))
s:taboption("general", Value, "remote1", translate("Remote1"))
s:taboption("general", Value, "remote2", translate("Remote2"))

return m
